<?php

namespace App\Models;

use App\Models\User;
use App\Models\SpendDetail;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class Spend extends Model
{
    use HasFactory;

    protected $guarded = [];
    protected $hidden = ['id', 'user_id', 'created_at', 'updated_at'];

    public function spend_details()
    {
        return $this->hasMany(SpendDetail::class, 'spend_id');
    }

    public function user()
    {
        return $this->belongsTo(User::class, 'id');
    }
}