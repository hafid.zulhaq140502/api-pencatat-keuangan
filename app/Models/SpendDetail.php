<?php

namespace App\Models;

use App\Models\Spend;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;


class SpendDetail extends Model
{
    use HasFactory;

    protected $guarded = [];
    protected $hidden = ['id', 'spend_id', 'created_at', 'updated_at'];

    public function spend()
    {
        return $this->belongsTo(Spend::class, 'id');
    }

    public function user()
    {
        return $this->belongsTo(Users::class, 'id');
    }
}