<?php

namespace App\Http\Resources;

use App\Http\Resources\SpendDetailResource;
use Illuminate\Http\Resources\Json\JsonResource;

class SpendResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {
        date_default_timezone_set('Asia/Jakarta');
        setlocale(LC_ALL, 'IND');

        return [
            'id' => $this->id,
            'month' => $this->month,
            'month_name' => strftime("%B", mktime(0, 0, 0, $this->month)),
            'year' => $this->year,
            'detail' => SpendDetailResource::collection($this->spend_details)

        ];
    }
}