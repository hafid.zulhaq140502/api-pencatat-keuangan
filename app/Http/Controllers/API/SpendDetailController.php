<?php

namespace App\Http\Controllers\API;

use Exception;
use App\Models\Spend;

use App\Models\SpendDetail;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\Auth;
use App\Http\Resources\SpendResource;
use Illuminate\Support\Facades\Validator;


class SpendDetailController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $spend_detail = Spend::with('spend_details')->orderBy('id', 'ASC')->get();
        $collectionSpend = SpendResource::collection($spend_detail);

        return response()->json(
            $collectionSpend,
            200
        );
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $spend_detail = Spend::with('spend_details')->findOrFail($id);

        return response()->json(
            $spend_detail,
            200
        );
    }

    /**
     * Store a newly Data berhasil ditambahkan resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $rules = [
            'month'                       => ['required', 'numeric', 'min:1', 'max:12'],
            'year'                        => ['required', 'numeric'],
            'spend_details.*.day'         => ['required', 'distinct'],
            'spend_details.*.total'       => ['required', 'numeric'],
            'spend_details.*.description' => 'nullable',
        ];

        $validator = Validator::make($request->all(), $rules);
        if ($validator->fails()) {
            return response()->json(['message' => $validator->errors()->first()], 422);
        }

        $spend = new Spend;
        $spend->user_id = Auth::id();
        $spend->month = $request->month;
        $spend->year = $request->year;
        $spend->save();

        foreach ($request->spend_details as $key) {

            $spend_detail = array(
                'spend_id' => $spend->id,
                'day' => $key['day'],
                'total' => $key['total'],
                'description' => $key['description']
            );

            $spend_detail = SpendDetail::create($spend_detail);
        }

        return response()->json([
            'message' => "Data berhasil ditambahkan"
        ], 200);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $spend_detail = Spend::findOrFail($id);

        $rules = [
            'month'                       => ['required', 'numeric', 'min:1', 'max:12'],
            'year'                        => ['required', 'numeric'],
            'spend_details.*.day'         => ['required', 'distinct'],
            'spend_details.*.total'       => ['required', 'numeric'],
            'spend_details.*.description' => 'nullable',
        ];

        $validator = Validator::make($request->all(), $rules);
        if ($validator->fails()) {
            return response()->json(['message' => $validator->errors()->first()], 422);
        }

        $spend_detail->update([

            'user_id' => Auth::id(),
            'month' => $request->month,
            'year' => $request->year
        ]);

        SpendDetail::where('spend_id', $id)->delete();

        foreach ($request->spend_details as $key) {
            $spend_detail = array(
                'spend_id' => $id,
                'day' => $key['day'],
                'total' => $key['total'],
                'description' => $key['description']
            );
            $spend_detail = SpendDetail::create($spend_detail);
        }
        return response()->json([
            'message' => "Data berhasil diubah"

        ], 200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try {
            $spend_detail = Spend::with('spend_details')->findOrFail($id)->delete();
            return response()->json([
                'message' => "Data berhasil dihapus",

            ], 200);
        } catch (Exception $e) {
            return response()->json([
                'message' => "Operasi gagal",

            ], 400);
        }
    }
}