<?php

namespace App\Http\Controllers\API;

use Exception;
use App\Models\Spend;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use App\Http\Resources\SpendResource;
use Illuminate\Support\Facades\Validator;

class SpendController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {

        $spend = Spend::with('spend_details')->orderBy('id', 'ASC')->get();
        return response()->json($spend, 200);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $spend = Spend::findOrFail($id);
        $spendCollection = new SpendResource($spend);

        return response()->json(
            $spendCollection,
            200
        );
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    // public function store(Request $request)
    // {

    //     $rules = [
    //         'month'       => ['required', 'numeric', 'min:1', 'max:12'],
    //         'year'        => ['required', 'numeric'],
    //     ];

    //     $validator = Validator::make($request->all(), $rules);
    //     if ($validator->fails()) {
    //         return response()->json(['message' => $validator->errors()->first()], 422);
    //     }

    //     $spend = new Spend;
    //     $spend->user_id = Auth::id();
    //     $spend->month = $request->month;
    //     $spend->year = $request->year;
    //     $spend->save();

    //     return response()->json([
    //         'message' => "Data berhasil ditambahkan"

    //     ], 200);
    // }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    // public function update(Request $request, $id)
    // {
    //     $spend = Spend::findOrFail($id);

    //     $rules = [
    //         'month'       => ['required', 'numeric', 'min:1', 'max:12'],
    //         'year'        => ['required', 'numeric'],
    //         'description' => 'nullable',
    //     ];

    //     $validator = Validator::make($request->all(), $rules);
    //     if ($validator->fails()) {
    //         return response()->json(['message' => $validator->errors()->first()], 422);
    //     }

    //     $spend->update([
    //         'user_id' => Auth::id(),
    //         'month' => $request->month,
    //         'year' => $request->year
    //     ]);

    //     return response()->json([
    //         'message' => "Data berhasil diubah",

    //     ], 200);
    // }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {

        try {
            $spend = Spend::findOrFail($id)->delete();
            return response()->json([
                'message' => "Data berhasil dihapus",

            ], 200);
        } catch (Exception $e) {
            return response()->json([
                'message' => "Operasi gagal",

            ], 400);
        }
    }
}