<?php

namespace App\Http\Controllers\API;

use App\Models\User;

use App\Models\Spend;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;
use App\Http\Resources\UserResource;
use Illuminate\Support\Facades\Validator;

class AuthController extends Controller
{
    public function register(Request $request)
    {
        $rules = [
            'username' => ['required', 'min:3'],
            'email' => ['required', 'email', 'max:255', 'unique:users'],
            'password' => ['required', 'min:6']
        ];

        $validator = Validator::make($request->all(), $rules);
        if ($validator->fails()) {
            return response()->json(['message' => $validator->errors()], 422);
        }

        $user = new User;
        $user->username = $request->username;
        $user->fullname = $request->fullname;
        $user->email = $request->email;
        $user->password = Hash::make($request->password);
        $user->save();

        return response()->json([
            'message' => "Regristasi berhasil"

        ], 200);
    }

    public function login(Request $request)
    {
        $user = User::where('username', $request->username)->first();

        if (!$user || !Hash::check($request->password, $user->password)) {

            return response()->json([
                'message' => "Identitas tersebut tidak cocok dengan data kami.",
            ], 401);
        }

        $token = $user->createToken('token')->plainTextToken;

        return response()->json([
            'message' => "Login berhasil",
            'user' => $user,
            'token' => $token
        ], 200);
    }

    public function getData(Request $request)
    {
        $user = Spend::get();
        $collectionUser = UserResource::collection($user);

        return response()->json(
            $collectionUser,
            200
        );
    }

    public function logout(Request $request)
    {
        $user = $request->user();
        $user->tokens()->delete();


        return response()->json([
            'message' => "Logout berhasil"
        ], 200);
    }
}