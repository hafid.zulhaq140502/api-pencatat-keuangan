<?php

namespace Database\Seeders;

use App\Models\User;
use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        User::create([
            'username' => 'admin',
            'fullname' => 'Admin Xxx',
            'email' => 'admin@gmail.com',
            'password' => bcrypt('password')
        ]);

        User::create([
            'username' => 'super admin',
            'fullname' => 'Super Admin Xxx',
            'email' => 'superadmin@gmail.com',
            'password' => bcrypt('password')
        ]);
    }
}