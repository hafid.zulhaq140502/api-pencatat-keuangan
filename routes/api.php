<?php

use App\Http\Controllers\API\AuthController;
use App\Http\Controllers\API\SpendController;
use App\Http\Controllers\API\SpendDetailController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::group(['middleware' => 'auth:sanctum'], function () {
    // spends
    Route::resource('/spends', SpendController::class)->except(['create', 'edit']);

    // spend_details
    Route::resource('/spend_details', SpendDetailController::class)->except(['create', 'edit']);

    // user
    Route::get('/get_data', [AuthController::class, 'getData']);
    Route::get('/logout', [AuthController::class, 'logout']);
});

Route::post('/login', [AuthController::class, 'login']);
Route::post('/register', [AuthController::class, 'register']);